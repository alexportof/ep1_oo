#ifndef CANDIDATE_HPP  
#define CANDIDATE_HPP
#include <fstream>
#include <string>

using namespace std;

class Candidate
{
private:
	string role;
	string state;
	string political_name;
public:
	string name;
	int quantity_of_votes; 
	string code;
	Candidate();
	Candidate(string role,string state,string political_name);
	~Candidate();

	string get_name();
	void set_name(string name);
	string get_role();
	void set_role(string role);
	string get_state();
	void set_state(string state);
	string get_political_name();
	void set_political_name(string political_name);
};


#endif