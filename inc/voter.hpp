#ifndef VOTER_HPP
#define VOTER_HPP

#include <string>

 using namespace std;

class Voter {

	private:
		int district_deputy_vote;
		int congressman_vote;
		int senator1_vote;
		int senator2_vote;
		int governor_vote;
		int president_vote;
		string voter_name;
		string district_deputy_name;
		string congressman_name;
		string senator1_name;
		string senator2_name;
		string governor_name;
		string president_name;

	public:	
		Voter();
		~Voter();
		string get_voter_name();
	   	void set_voter_name(string voter_name);
	   	int get_district_deputy_vote();
		void set_district_deputy_vote(int district_deputy_vote);
		int get_congressman_vote();
		void set_congressman_vote(int congressman_vote);
		int get_senator1_vote();
		void set_senator1_vote(int senator1_vote);
		int get_senator2_vote();
		void set_senator2_vote(int senator2_vote);
		int get_governor_vote();
		void set_governor_vote(int governor_vote);
		int get_president_vote();
		void set_president_vote(int president_vote);
		string get_district_deputy_name();
		void set_district_deputy_name(string district_deputy_name);
		string get_congressman_name();
		void set_congressman_name(string congressman_name);
		string get_senator1_name();
		void set_senator1_name(string senator1_name);
		string get_senator2_name();
		void set_senator2_name(string senator2_name);
		string get_governor_name();
		void set_governor_name(string governor_name);
		string get_president_name();
		void set_president_name(string president_name);
	   	
};


#endif