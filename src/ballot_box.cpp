#include "ballot_box.hpp"
#include "candidate.hpp"
#include "voter.hpp" 		
#include <iostream>
#include <fstream>
#include <string>


using namespace std;

Ballot_box::Ballot_box(){}
Ballot_box::~Ballot_box(){}

string Ballot_box::catch_district_deputy_vote(Voter voter){
	int vote;
	cout << "Qual seu voto para Deputado Distrital? Se quiser votar em branco,digite 0 "<< endl;
		
	while(1){
		cin >> vote;
		if(vote < 10000 && vote != 0 && vote != 1 && vote != 2| vote > 100000){
			cout << "Deputado Distrital possui 5 numeros,por favor coloque de novo" << endl;
		}
		else if(vote == 0){
			voter.set_district_deputy_vote(vote);
			break;
		}
		else{
			voter.set_district_deputy_vote(vote);
			cout << "Para CONFIRMAR tecle 1 e CANCELAR tecle 2" << endl;
			int confirm;
			cin >> confirm;
			if(confirm == 1){}
			else if(confirm == 2){
				cout << "Digite de novo seu voto" << endl;
				cin >> vote;
				voter.set_district_deputy_vote(vote);
			} 
			string voto;
			voto = to_string(vote);
			return voto;
			break;
			}

		} 
}
string Ballot_box::catch_congressman_vote(Voter voter){
	int vote;
	cout << "E para Deputado Federal? Se quiser votar em branco,digite 0 " << endl;
	while(1){
		cin >> vote;
		if(vote < 1000 && (vote != 0 && vote != 1 && vote != 2) || vote > 10000){
			cout << "Deputado Federal possui 4 numeros,por favor coloque de novo" << endl;
		}
		else if(vote == 0){
			voter.set_congressman_vote(vote);
			break;
		}	
		else{
			voter.set_congressman_vote(vote);
			cout << "Para CONFIRMAR tecle 1 e CANCELAR tecle 2" << endl;
			int confirm;
			cin >> confirm;
			if(confirm == 1){}
			else if(confirm == 2){
				cout << "Digite de novo seu voto" << endl;
				cin >> vote;
				voter.set_congressman_vote(vote);
			} 
			string voto;
			voto = to_string(vote);
			return voto;
			break;
		}
	}
}
string Ballot_box::catch_senator1_vote(Voter voter){
	int vote;
	cout << "Primeiro Senador? Se quiser votar em branco,digite 0 " << endl;
	while(1){
		cin >> vote;
		if(vote < 100 &&(vote != 0 && vote != 1 && vote != 2) || vote > 1000){
			cout << " Senador possui 3 numeros,por favor coloque de novo" << endl;
		}
		else if(vote == 0){
			voter.set_senator1_vote(vote);
			break;
		}	
		else{
			Ballot_box::senator_vote = vote;
			voter.set_senator1_vote(vote);
			
			cout << "Para CONFIRMAR tecle 1 e CANCELAR tecle 2" << endl;
			int confirm;
			cin >> confirm;
			if(confirm == 1){}
			else if(confirm == 2){
				cout << "Digite de novo seu voto" << endl;
				cin >> vote;
				voter.set_senator1_vote(vote);
			} 

			string voto;
			voto = to_string(vote);
			return voto;
			break;
		}
	}
}
string Ballot_box::catch_senator2_vote(Voter voter){
	int vote;
	cout << "Segundo Senador? Se quiser votar em branco,digite 0 " << endl;
	while(1){
		cin >> vote;
		if(vote < 100 && (vote != 0 && vote != 1 && vote != 2) || vote > 1000){
			cout << " Senador possui 3 numeros,por favor coloque de novo" << endl;
		}
		else if(vote == Ballot_box::senator_vote){
			cout << "Não é possivel votar no mesmo senador,por favor coloque de novo" << endl;
		}
		else if(vote == 0){
			voter.set_senator2_vote(vote);
			break;
		}
		else{
			voter.set_senator2_vote(vote);
			
			cout << "Para CONFIRMAR tecle 1 e CANCELAR tecle 2" << endl;
			int confirm;
			cin >> confirm;
			if(confirm == 1){}
			else if(confirm == 2){
				cout << "Digite de novo seu voto" << endl;
				cin >> vote;
				voter.set_senator2_vote(vote);
			} 

			string voto;
			voto = to_string(vote);
			return voto;
			break;
		}
	}
}
string Ballot_box::catch_governor_vote(Voter voter){
	int vote;
	cout << "Governador? Se quiser votar em branco,digite 0 " << endl;
	while(1){
		cin >> vote;
		if(vote < 10 && (vote != 0 && vote != 1 && vote != 2) || vote > 100){
			cout << " Governador possui 2 numeros,por favor coloque de novo" << endl;
		}
		else if(vote == 0){
			voter.set_governor_vote(vote);
			break;
		}
		else{
			voter.set_governor_vote(vote);
			cout << "Para CONFIRMAR tecle 1 e CANCELAR tecle 2" << endl;
			int confirm;
			cin >> confirm;
			if(confirm == 1){}
			else if(confirm == 2){
				cout << "Digite de novo seu voto" << endl;
				cin >> vote;
				voter.set_governor_vote(vote);
			} 	
				string voto;
				voto = to_string(vote);
				return voto;
				break;
		}
	}
}
string Ballot_box::catch_president_vote(Voter voter){
	int vote;
	cout << "Presidente?" << endl;
while(1){
		cin >> vote;
		if(vote < 10 && (vote != 0 && vote != 1 && vote != 2) || vote > 100){
			cout << " Presidente possui 2 numeros,por favor coloque de novo" << endl;
		}
		else if(vote == 0){
			voter.set_president_vote(vote);
			break;
		}
		else{
			voter.set_president_vote(vote);
			
			cout << "Para CONFIRMAR tecle 1 e CANCELAR tecle 2" << endl;
			int confirm;
			cin >> confirm;
			if(confirm == 1){}
			else if(confirm == 2){
				cout << "Digite de novo seu voto" << endl;
				cin >> vote;
				voter.set_president_vote(vote);
			} 

			string voto;
			voto = to_string(vote);
			return voto;
			break;
		}
	}
}
void Ballot_box::show_candidate(Candidate candidate){
			cout << "Nome: " << candidate.name << endl;
			cout << "Cargo: " << candidate.get_role() << endl;
			cout << "Estado: " << candidate.get_state() << endl;
			cout << "Partido: " << candidate.get_political_name() << endl << endl;	
}

void Ballot_box::start_voting(Voter *voter,Candidate *DFcandidates,Candidate *BRcandidates){
 	
	int i =0;
	string district_deputy_vote,congressman_vote,senator1_vote,senator2_vote,governor_vote,president_vote;

 	district_deputy_vote = catch_district_deputy_vote(*voter);
 	 for(int i=0;i<1183;i++){
 	 	if(district_deputy_vote == DFcandidates[i].code){
 	 		DFcandidates[i].quantity_of_votes++;
 	 		show_candidate(DFcandidates[i]);
 	 		voter->set_district_deputy_name(DFcandidates[i].name);
 	 	}
 	 }
 	congressman_vote = catch_congressman_vote(*voter);
 	for(i=0;i<1183;i++){
 		if(congressman_vote == DFcandidates[i].code){
 			DFcandidates[i].quantity_of_votes++;
 			show_candidate(DFcandidates[i]);
 			voter->set_congressman_name(DFcandidates[i].name);
 		}
 	}
	senator1_vote = catch_senator1_vote(*voter);
 	for(i=0;i<1183;i++){
 		if(senator1_vote == DFcandidates[i].code){
 			DFcandidates[i].quantity_of_votes++;
 			show_candidate(DFcandidates[i]);
 			voter->set_senator1_name(DFcandidates[i].name);
 		}
 	} 
 	senator2_vote = catch_senator2_vote(*voter);
 	for(i=0;i<1183;i++){
 		if(senator2_vote == DFcandidates[i].code){
 			DFcandidates[i].quantity_of_votes++;
 			show_candidate(DFcandidates[i]);
 			voter->set_senator2_name(DFcandidates[i].name);
 		}
 	}
 	governor_vote = catch_governor_vote(*voter);
 	for(i=0;i<1183;i++){
 		if(governor_vote == DFcandidates[i].code){
 			DFcandidates[i].quantity_of_votes++;
 			show_candidate(DFcandidates[i]);
 			voter->set_governor_name(DFcandidates[i].name);
 		}
 	}
 	president_vote = catch_president_vote(*voter);
 	for(i=0;i<13;i++){
 		if(president_vote == BRcandidates[i].code){
 			BRcandidates[i].quantity_of_votes++;
 			show_candidate(BRcandidates[i]);
 			voter->set_president_name(BRcandidates[i].name);
 		}
 	}

}
void Ballot_box::counting_of_votes(Candidate DFcandidate[],Candidate BRcandidate[]){
	Candidate district_deputy[968],congressman[185],senator[20],governor[31],president[13];
	Candidate winner_district_deputy,winner_congressman,winner_senator1,winner_senator2,winner_governor,winner_president;
	int code,j;
		j=0;
	for(int i=0;i<1183;i++){
		code = stoi(DFcandidate[i].code);
		if(code > 10000){
			district_deputy[j] = DFcandidate[i];
			j++;
		}
	}
		j=0;
	for(int i=0;i<1183;i++){
		code = stoi(DFcandidate[i].code);
		if (code > 1000 && code < 10000){
			congressman[j] = DFcandidate[i];
			j++;
		}
	}
		j=0;
	for(int i=0;i<1183;i++){
		code = stoi(DFcandidate[i].code);
		if (code > 100 && code < 1000){
			senator[j] = DFcandidate[i];
			j++;
		}
	}
		j=0;
	for(int i=0;i<1183;i++){
		code = stoi(DFcandidate[i].code);
		if(code > 10 && code < 100){
			governor[j] = DFcandidate[i];
			j++;
		}
	}
		j=0;
	for(int i=0;i<13;i++){
		code = stoi(BRcandidate[i].code	);
		president[j] = BRcandidate[i];
		j++;
	}

	for (int i = 0; i < 968; i++){
		for(int j=0;j<968;j++){
			if(district_deputy[i].quantity_of_votes > district_deputy[j].quantity_of_votes){
				winner_district_deputy = district_deputy[i];
			} 
		}
	}
	for(int i=0;i < 185;i++){
		for(int j=0;j<185;j++){
			if(congressman[i].quantity_of_votes > congressman[j].quantity_of_votes){
				winner_congressman = congressman[i];
			}
		}
	}	
	for(int i=0;i<20;i++){
		for(int j=0;j<20;j++){
			if(senator[i].quantity_of_votes > senator[j].quantity_of_votes){
				winner_senator1 = senator[i];
			}
		}
	}
	for(int i=0;i<20;i++){
		for(int j=0;j<20;j++){
			if(senator[i].quantity_of_votes != winner_senator1.quantity_of_votes){
				if(senator[i].quantity_of_votes > senator[j].quantity_of_votes){
					winner_senator2 = senator[i];
				}
			}
		}
	}
	for(int i=0;i<31;i++){
		for(int j=0;j<31;j++){
			if(governor[i].quantity_of_votes > governor[j].quantity_of_votes){
				winner_governor = governor[i];
			}
		}
	}
	for(int i=0;i<13;i++){
		for(int j=0;j<13;j++){
			if(president[i].quantity_of_votes > president[j].quantity_of_votes){
				winner_president = president[i];
			}
		}
	}
	 cout << endl << "OS ELEITOS NESSA URNA SÃO : " << endl;
	cout << "DEPUTADO DISTRITAL: " << winner_district_deputy.name << endl;
	cout << "DEPUTADO FEDERAL: " << winner_congressman.name << endl;
	cout << "SENADOR 1: " << winner_senator1.name << endl;
	cout << "SENADOR 2: " << winner_senator2.name << endl;
	cout << "GOVERNADOR: " << winner_governor.name << endl;
	cout << "PRESIDENTE: " << winner_president.name << endl;

	
}