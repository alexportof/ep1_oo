#include "voter.hpp"
#include <iostream>
#include <string>

using namespace std;

Voter::Voter(){
	string name;
	cout << "Digite seu nome,sem espacos,por favor" << endl;
	cin >> name;
	set_voter_name(name);
}
Voter::~Voter(){}

string Voter::get_voter_name(){
	return voter_name;
}
void Voter::set_voter_name(string voter_name){
	this->voter_name = voter_name;
}
	int Voter::get_district_deputy_vote(){
	return district_deputy_vote;
}
void Voter::set_district_deputy_vote(int district_deputy_vote){
	this->district_deputy_vote = district_deputy_vote;
}
int Voter::get_congressman_vote(){
	return congressman_vote;
}
void Voter::set_congressman_vote(int congressman_vote){
	this->congressman_vote = congressman_vote;
}
int Voter::get_senator1_vote(){
	return senator1_vote;
}
void Voter::set_senator1_vote(int senator1_vote){
	this-> senator1_vote = senator1_vote;
}
int Voter::get_senator2_vote(){
	return senator2_vote;
}
void Voter::set_senator2_vote(int senator2_vote){
	this-> senator2_vote = senator2_vote;
}
int Voter::get_governor_vote(){
	return governor_vote;
}
void Voter::set_governor_vote(int governor_vote){
	this->governor_vote = governor_vote;
}
int Voter::get_president_vote(){
	return president_vote;
}
void Voter::set_president_vote(int president_vote){
	this->president_vote = president_vote;
}
string Voter::get_district_deputy_name(){
	return district_deputy_name;
}
void Voter::set_district_deputy_name(string district_deputy_name){
	this-> district_deputy_name = district_deputy_name;
}
string Voter::get_congressman_name(){
	return congressman_name;
}
void Voter::set_congressman_name(string congressman_name){
	this->congressman_name = congressman_name;
}
string Voter::get_senator1_name(){
	return senator1_name;
}
void Voter::set_senator1_name(string senator1_name){
	this-> senator1_name = senator1_name;
}
string Voter::get_senator2_name(){
	return senator2_name;
}
void Voter::set_senator2_name(string senator2_name){
	this-> senator2_name = senator2_name;
}
string Voter::get_governor_name(){
	return governor_name;
}
void Voter::set_governor_name(string governor_name){
	this->governor_name = governor_name;
}
string Voter::get_president_name(){
	return president_name;
}
void Voter::set_president_name(string president_name){
	this->president_name = president_name;
}